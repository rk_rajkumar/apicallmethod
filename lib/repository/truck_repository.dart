import 'dart:async';

import 'package:flutterlorryapikhit/models/Info.dart';
import 'package:flutterlorryapikhit/networking/ApiProvider.dart';
import 'package:flutterlorryapikhit/view/newmodel.dart';

class TruckRepository {
  ApiProvider _provider = ApiProvider();

  Future<Information> fetchChuckCategoryData() async {
    final response = await _provider.get("get_vehicles", 0);
    return Information.fromJson(response);
  }

  Future<newmodel> fetchfirstapi(String attributeKey) async {
    final response = await _provider.get("get_attribute/$attributeKey", 0);
    return newmodel.fromJson(response);
  }
}
