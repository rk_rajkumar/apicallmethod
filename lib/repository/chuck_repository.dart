import 'dart:async';

import 'package:flutterlorryapikhit/models/chuck_response.dart';
import 'package:flutterlorryapikhit/networking/ApiProvider.dart';

class ChuckRepository {
  ApiProvider _provider = ApiProvider();

  Future<chuckResponse> fetchChuckJoke(String category) async {
    final response =
        await _provider.get("jokes/random?category=" + category, 0);
    return chuckResponse.fromJson(response);
  }
}
