import 'dart:async';

import 'package:flutterlorryapikhit/models/chuck_categories.dart';
import 'package:flutterlorryapikhit/networking/ApiProvider.dart';

class ChuckCategoryRepository {
  ApiProvider _provider = ApiProvider();

  Future<chuckCategories> fetchChuckCategoryData() async {
    final response = await _provider.get("jokes/categories", 0);
    return chuckCategories.fromJson(response);
  }
}
