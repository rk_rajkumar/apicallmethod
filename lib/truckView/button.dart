import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  final Widget child;
  final minWidth;
  final Height;
  final Width;
  final borderRadius;
  final color;
  final prefixIcon;
  final suffixIcon;
  final String textValue;
  final fontWeight;
  final fontSize;
  final bgcolor;
  final Function onPressed;
  final Icons getIcon;
  final Image image;
  final String getImageName;
  final double getImageHeight;
  final double getImageWidth;

  const Button({
    Key key,
    this.child,
    this.Height,
    this.minWidth,
    this.onPressed,
    this.prefixIcon,
    this.suffixIcon,
    this.color,
    this.bgcolor,
    this.textValue,
    this.fontWeight,
    this.image,
    this.fontSize,
    this.Width,
    this.borderRadius,
    this.getIcon,
    this.getImageName,
    this.getImageHeight,
    this.getImageWidth,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Height,
      width: Width,
      decoration: BoxDecoration(
        color: bgcolor ?? Colors.lightBlue,
        borderRadius: borderRadius,
      ),
      child: Container(
        child: InkWell(
          onTap: onPressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Visibility(
                visible: false,
                child: Image.asset(
                  getImageName ?? "",
                  height: getImageHeight ?? 30.0,
                  width: getImageWidth ?? 25.0,
                ),
              ),
              SizedBox(
                width: 3.0,
              ),
              Text(
                textValue ?? "",
                style: TextStyle(
                    color: color, fontWeight: fontWeight, fontSize: fontSize),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class Button_with_icon extends StatelessWidget {
  final Widget child;
  final minWidth;
  final Height;
  final Width;
  final borderRadius;
  final color;
  final prefixIcon;
  final suffixIcon;
  final textValue;
  final fontWeight;
  final fontSize;
  final bgcolor;
  final Function onPressed;
  final Icons getIcon;

  const Button_with_icon({
    Key key,
    @required this.child,
    this.Height,
    this.minWidth,
    this.onPressed,
    this.prefixIcon,
    this.suffixIcon,
    this.color,
    this.bgcolor,
    this.textValue,
    this.fontWeight,
    this.fontSize,
    this.Width,
    this.borderRadius,
    this.getIcon,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Container(
      height: Height,
      width: Width,
      decoration: BoxDecoration(
        color: bgcolor,
        borderRadius: borderRadius,
      ),
      child: Container(
        child: InkWell(
          onTap: onPressed,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                textValue ?? "",
                style: TextStyle(
                    color: color, fontWeight: fontWeight, fontSize: fontSize),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//class Button extends StatefulWidget {
//  final Widget child;
//  final  minWidth;
//  final  height;
//  final  width;
//  final shape;
//  final color;
//  final Function onPressed;
//
//  const Button({
//    Key key,
//    @required this.child,
//    this.height = 50.0,
//    this.minWidth,
//    this.onPressed,
//    this.color,
//    this.width,
//    this.shape,
//  }) : super(key: key);
//  @override
//  _ButtonState createState() => _ButtonState();
//}
//
//class _ButtonState extends State<Button> {
//  @override
//  Widget build(BuildContext context) {
//    return Container(
//      height: widget.height,
//      width: widget.width,
//    child: InkWell(
//        onTap: onPressed,
//        child: Center(
//          child: child,
//        ))
//
//  );
//}

//
//child: MaterialButton(
//onPressed: () => {
//Navigator.push(
//context,
//MaterialPageRoute(
//builder: (context) => UserRegister()))
//},
//textColor: Colors.black,
//shape: RoundedRectangleBorder(
//borderRadius: BorderRadius.circular(3),
//),
//color: Colors.grey,
//height: 60,
//minWidth: 1000,
//child: Text(
//'No Account? Sign Up',
//style: TextStyle(fontSize: 18),
//),
//),
