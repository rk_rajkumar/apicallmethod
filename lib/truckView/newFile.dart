import 'package:flutter/material.dart';

class NewDesign extends StatefulWidget {
  @override
  _NewDesignState createState() => _NewDesignState();
}

class _NewDesignState extends State<NewDesign> {
  int _n = 0;
  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(title: new Text("Number Counsst")),
      body: new Container(
        padding: EdgeInsets.all(8.0),
        height: 50,
        child: new Center(
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Icon(
                Icons.event,
                color: Colors.black,
              ),
              new Text('The Defalut Text',
                  style: new TextStyle(fontSize: 18.0)),
              Container(
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    border: Border.all(color: Colors.red)),
                child: Row(
                  children: [
                    Container(
                      child: new FloatingActionButton(
                        onPressed: add,
                        child: new Icon(
                          Icons.add,
                          color: Colors.black,
                        ),
                        backgroundColor: Colors.white,
                      ),
                    ),
                    new Text('$_n', style: new TextStyle(fontSize: 30.0)),
                    new FloatingActionButton(
                      onPressed: minus,
                      child: new Icon(
                          const IconData(0xe15b, fontFamily: 'MaterialIcons'),
                          color: Colors.black),
                      backgroundColor: Colors.white,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  BoxDecoration myBoxDecoration() {
    return BoxDecoration(
      border: Border.all(),
    );
  }

  void minus() {
    setState(() {
      if (_n != 0) _n--;
    });
  }

  void add() {
    setState(() {
      _n++;
    });
  }
}
