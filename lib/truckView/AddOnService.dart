import 'package:flutter/material.dart';

class AddOnServicePage extends StatefulWidget {
  @override
  _AddOnServicePageState createState() => _AddOnServicePageState();
}

class _AddOnServicePageState extends State<AddOnServicePage> {
  List<String> _truckTypes = <String>[
    '',
    'Small Cars',
    'XUV',
    'Large',
    'Tempo'
  ];
  String _truck = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("BO/062020/10928"),
          leading: IconButton(
            icon: const Icon(Icons.arrow_back_ios),
            tooltip: 'Next page',
            onPressed: () {},
          ),
          backgroundColor: Colors.red,
        ),
        body: new SafeArea(
            child: new Form(
                child: new ListView(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          children: <Widget>[
            new FormField(
              builder: (FormFieldState state) {
                return firstContainer(
                  state,
                  32.0,
                  IconButton(
                    icon: Icon(Icons.keyboard_arrow_down),
                    onPressed: () {},
                  ),
                );
              },
            ),
            new FormField(
              builder: (FormFieldState state) {
                return firstContainer(
                  state,
                  10.0,
                  IconButton(
                    icon: Icon(Icons.keyboard_arrow_down),
                    onPressed: () {},
                  ),
                );
              },
            ),
            new FormField(
              builder: (FormFieldState state) {
                return firstContainer(
                  state,
                  10.0,
                  IconButton(
                    icon: Icon(Icons.location_on),
                    onPressed: () {},
                  ),
                );
              },
            ),
            new FormField(
              builder: (FormFieldState state) {
                return firstContainer(
                  state,
                  10.0,
                  IconButton(
                    icon: Icon(Icons.location_on),
                    onPressed: () {},
                  ),
                );
              },
            ),
          ],
        ))));
  }

  Container firstContainer(
      FormFieldState state, double topValue, Widget dataIcon) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      margin: EdgeInsets.only(top: topValue),
      height: 56,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 13.0,
            color: Colors.black.withOpacity(.5),
            offset: Offset(1.0, 1.0),
          ),
        ],
        border: Border.all(width: 1, color: Colors.grey[600]),
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      child: new DropdownButtonHideUnderline(
        child: new DropdownButton(
          value: _truck,
          isDense: true,
          icon: dataIcon,
          onChanged: (String newValue) {
            setState(() {
//                          newContact.favoriteColor = newValue;
              _truck = newValue;
              state.didChange(newValue);
            });
          },
          items: _truckTypes.map((String value) {
            return new DropdownMenuItem(
              value: value,
              child: new Text(value),
            );
          }).toList(),
        ),
      ),
    );
  }
}
