import 'package:flutter/material.dart';
import 'package:flutterlorryapikhit/truckView/button.dart';

class TabViewSample extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: DefaultTabController(
        length: 2,
        child: Scaffold(
          appBar: AppBar(
            title: Text('Flutter Tabs Demo'),
            bottom: TabBar(
              tabs: [Tab(text: "Search & Add"), Tab(text: "Add new Customer")],
            ),
          ),
          body: TabBarView(
            children: [
              FScreen(),
              SScreen(),
            ],
          ),
        ),
      ),
    );
  }
}

class FScreen extends StatefulWidget {
  @override
  _FScreenState createState() => _FScreenState();
}

class _FScreenState extends State<FScreen> {
  int _radioValue1 = -1;
  int _radioValue2 = -1;

  int correctScore = 0;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        padding: const EdgeInsets.all(8.0),
        child: Column(
          children: [
            Align(
              alignment: Alignment.centerLeft,
              child: Text(
                'Search & Add Customer Information',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 10.0),
              child: Text(
                'It is a contact tab, which is responsible for displaying the contacts stored in your mobile',
                style: TextStyle(fontSize: 18.0),
              ),
            ),
            Row(
              children: [
                new Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text(
                  'Login Email',
                  style: new TextStyle(fontSize: 16.0),
                ),
                new Radio(
                  value: 1,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text(
                  'NRIC / Passport',
                  style: new TextStyle(
                    fontSize: 16.0,
                  ),
                ),
              ],
            ),
            new FormField(
              builder: (FormFieldState state) {
                return firstContainer(
                  state,
                  32.0,
                  IconButton(
                    icon: Icon(Icons.keyboard_arrow_down),
                    onPressed: () {},
                  ),
                );
              },
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: new Text(
                'It is a contact tab, which is responsible for displaying the contacts stored in your mobile',
                style: new TextStyle(
                  fontSize: 14.0,
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Button(
                  getImageName: "assets/images/ic_approval.png",
                  getImageHeight: 40.0,
                  getImageWidth: 40.0,
                  Width: 150.0,
                  Height: 40.0,
                  textValue: "FIND",
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
                  onPressed: () {},
                  borderRadius: BorderRadius.circular(40),
                  bgcolor: Colors.red,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: new Text(
                'It is a contact tab, which is responsible for displaying the contacts stored in your mobile',
                style: new TextStyle(fontSize: 20.0, color: Colors.green),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: new Text(
                'It is a contact tab, which is responsible for displaying the contacts stored in your mobile',
                style: new TextStyle(fontSize: 14.0, color: Colors.black),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Button(
                  Width: 120.0,
                  Height: 30.0,
                  textValue: "SEND OTP ",
                  color: Colors.white,
                  fontWeight: FontWeight.normal,
                  fontSize: 12.0,
                  onPressed: () {},
                  borderRadius: BorderRadius.circular(50),
                  bgcolor: Colors.brown,
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: 40.0),
              child: new Text(
                'It is a contact tab, which is responsible for displaying the contacts stored in your mobile',
                style: new TextStyle(fontSize: 14.0, color: Colors.black),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Button(
                  Width: 150.0,
                  Height: 40.0,
                  textValue: "Key-in OTP",
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
//child: Text("LOG IN",style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold,fontSize: 14),),
//width: halfMediaWidth / 2,
                  onPressed: () {},
                  borderRadius: BorderRadius.circular(50),
                  bgcolor: Colors.amberAccent,
                ),
              ),
            ),
            Align(
              alignment: Alignment.centerLeft,
              child: Container(
                margin: EdgeInsets.only(top: 20.0),
                child: Button(
                  getImageName: "assets/images/ic_approval.png",
                  getImageHeight: 40.0,
                  getImageWidth: 40.0,
                  Width: 150.0,
                  Height: 40.0,
                  textValue: "SUBMIT",
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.0,
                  onPressed: () {},
                  borderRadius: BorderRadius.circular(40),
                  bgcolor: Colors.red,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  Container firstContainer(
      FormFieldState state, double topValue, Widget dataIcon) {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      margin: EdgeInsets.only(top: topValue),
      height: 56,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 13.0,
            color: Colors.black.withOpacity(.5),
            offset: Offset(1.0, 1.0),
          ),
        ],
        border: Border.all(width: 1, color: Colors.grey[600]),
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
    );
  }
}

class SScreen extends StatefulWidget {
  @override
  _SScreenState createState() => _SScreenState();
}

class _SScreenState extends State<SScreen> {
  int _radioValue1 = -1;
  int _radioValue2 = -1;
  List<String> _truckTypes = <String>[
    '',
    'Small Cars',
    'XUV',
    'Large',
    'Tempo'
  ];
  String _truck = '';

  int correctScore = 0;
  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        padding: EdgeInsets.all(8.0),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.only(top: 8.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Add New Customer Information',
                  style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
                ),
              ),
            ),
            Row(
              children: [
                new Radio(
                  value: 0,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text(
                  'Malaysian',
                  style: new TextStyle(fontSize: 16.0),
                ),
                new Radio(
                  value: 1,
                  groupValue: _radioValue1,
                  onChanged: _handleRadioValueChange1,
                ),
                new Text(
                  'Foreigner',
                  style: new TextStyle(
                    fontSize: 16.0,
                  ),
                ),
              ],
            ),
            Container(
              margin: EdgeInsets.all(5.0),
              child: Theme(
                data:
                    Theme.of(context).copyWith(splashColor: Colors.transparent),
                child: TextField(
                  autofocus: false,
                  style: TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.white,
                    hintText: 'Key-in NRIC / Passport *',
                    hintStyle: TextStyle(color: Colors.black),
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 8.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color(0xFFbdc6cf), width: 2.0),
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(8.0),
              child: Theme(
                data:
                    Theme.of(context).copyWith(splashColor: Colors.transparent),
                child: TextField(
                  autofocus: false,
                  style: TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.white,
                    hintStyle: TextStyle(color: Colors.black),
                    hintText: 'Name *',
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 8.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color(0xFFbdc6cf), width: 2.0),
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(8.0),
              child: Theme(
                data:
                    Theme.of(context).copyWith(splashColor: Colors.transparent),
                child: TextField(
                  autofocus: false,
                  style: TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.white,
                    hintStyle: TextStyle(color: Colors.black),
                    hintText: 'Email Address *',
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 8.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color(0xFFbdc6cf), width: 2.0),
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(8.0),
              child: Theme(
                data:
                    Theme.of(context).copyWith(splashColor: Colors.transparent),
                child: TextField(
                  autofocus: false,
                  style: TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.white,
                    hintStyle: TextStyle(color: Colors.black),
                    hintText: 'Mobile Number *',
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 8.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color(0xFFbdc6cf), width: 2.0),
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(8.0),
              child: Theme(
                data:
                    Theme.of(context).copyWith(splashColor: Colors.transparent),
                child: TextField(
                  autofocus: false,
                  style: TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    filled: true,
                    fillColor: Colors.white,
                    hintText: 'Address',
                    hintStyle: TextStyle(color: Colors.black),
                    contentPadding: const EdgeInsets.only(
                        left: 14.0, bottom: 8.0, top: 8.0),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                      borderRadius: BorderRadius.circular(25.7),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(
                          color: Color(0xFFbdc6cf), width: 2.0),
                      borderRadius: BorderRadius.circular(25.0),
                    ),
                  ),
                ),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Theme(
                      data: Theme.of(context)
                          .copyWith(splashColor: Colors.transparent),
                      child: TextField(
                        autofocus: false,
                        style:
                            TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                        decoration: InputDecoration(
                          border: InputBorder.none,
                          filled: true,
                          fillColor: Colors.white,
                          hintText: 'City',
                          hintStyle: TextStyle(color: Colors.black),
                          contentPadding: const EdgeInsets.only(
                              left: 14.0, bottom: 8.0, top: 8.0),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                            borderRadius: BorderRadius.circular(25.7),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(
                                color: Color(0xFFbdc6cf), width: 2.0),
                            borderRadius: BorderRadius.circular(25.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                Expanded(
                  child: new FormField(
                    builder: (FormFieldState state) {
                      return firstContainer(
                        state,
                        0.0,
                        IconButton(
                          icon: Icon(Icons.keyboard_arrow_down),
                          onPressed: () {},
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
            Container(
              padding: EdgeInsets.all(8.0),
              child: Row(
                children: [
                  Expanded(
                    child: new FormField(
                      builder: (FormFieldState state) {
                        return firstContainer(
                          state,
                          0.0,
                          IconButton(
                            icon: Icon(Icons.keyboard_arrow_down),
                            onPressed: () {},
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 8.0, top: 8.0),
              child: Align(
                alignment: Alignment.centerLeft,
                child: Text(
                  'Select User Account Usage',
                  style:
                      TextStyle(fontSize: 15.0, fontWeight: FontWeight.normal),
                ),
              ),
            ),
            Column(
              children: [
                Row(
                  children: [
                    new Radio(
                      value: 0,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    new Text(
                      'Personal Use Only',
                      style: new TextStyle(fontSize: 16.0, color: Colors.black),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 50.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: new Text(
                      'If customer using this account for his/her \n personal bookings ',
                      style: new TextStyle(fontSize: 12.0),
                    ),
                  ),
                ),
                Row(
                  children: [
                    new Radio(
                      value: 0,
                      groupValue: _radioValue1,
                      onChanged: _handleRadioValueChange1,
                    ),
                    new Text(
                      'Commercial & Personal',
                      style: new TextStyle(fontSize: 16.0, color: Colors.black),
                    ),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(left: 50.0),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: new Text(
                      'If customer using this account for his/her \n personal bookings ',
                      style: new TextStyle(fontSize: 12.0),
                    ),
                  ),
                ),
                Row(
                  children: [
                    Checkbox(
                      value: isChecked,
                      onChanged: (value) {
                        toggleCheckbox(value);
                      },
                      activeColor: Colors.pink,
                      checkColor: Colors.white,
                      tristate: false,
                    ),
                    Text(
                      'Add Company Information',
                      style: TextStyle(fontSize: 15),
                    )
                  ],
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: Theme(
                    data: Theme.of(context)
                        .copyWith(splashColor: Colors.transparent),
                    child: TextField(
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Name of the Company *',
                        hintStyle: TextStyle(color: Colors.black),
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xFFbdc6cf), width: 2.0),
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: new FormField(
                          builder: (FormFieldState state) {
                            return firstContainer(
                              state,
                              0.0,
                              IconButton(
                                icon: Icon(Icons.keyboard_arrow_down),
                                onPressed: () {},
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: Theme(
                    data: Theme.of(context)
                        .copyWith(splashColor: Colors.transparent),
                    child: TextField(
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Registration Number (if any)',
                        hintStyle: TextStyle(color: Colors.black),
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xFFbdc6cf), width: 2.0),
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: Theme(
                    data: Theme.of(context)
                        .copyWith(splashColor: Colors.transparent),
                    child: TextField(
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Phone Number *',
                        hintStyle: TextStyle(color: Colors.black),
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xFFbdc6cf), width: 2.0),
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: Theme(
                    data: Theme.of(context)
                        .copyWith(splashColor: Colors.transparent),
                    child: TextField(
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Email Address *',
                        hintStyle: TextStyle(color: Colors.black),
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xFFbdc6cf), width: 2.0),
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                    ),
                  ),
                ),
                Container(
                  margin: EdgeInsets.all(8.0),
                  child: Theme(
                    data: Theme.of(context)
                        .copyWith(splashColor: Colors.transparent),
                    child: TextField(
                      autofocus: false,
                      style:
                          TextStyle(fontSize: 15.0, color: Color(0xFFbdc6cf)),
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        filled: true,
                        fillColor: Colors.white,
                        hintText: 'Address *',
                        hintStyle: TextStyle(color: Colors.black),
                        contentPadding: const EdgeInsets.only(
                            left: 14.0, bottom: 8.0, top: 8.0),
                        focusedBorder: OutlineInputBorder(
                          borderSide: BorderSide(color: Color(0xFFbdc6cf)),
                          borderRadius: BorderRadius.circular(25.7),
                        ),
                        enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(
                              color: Color(0xFFbdc6cf), width: 2.0),
                          borderRadius: BorderRadius.circular(25.0),
                        ),
                      ),
                    ),
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(16.0),
                        child: Theme(
                          data: Theme.of(context)
                              .copyWith(splashColor: Colors.transparent),
                          child: TextField(
                            autofocus: false,
                            style: TextStyle(
                                fontSize: 15.0, color: Color(0xFFbdc6cf)),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              filled: true,
                              fillColor: Colors.white,
                              hintText: 'City *',
                              hintStyle: TextStyle(color: Colors.black),
                              contentPadding: const EdgeInsets.only(
                                  left: 14.0, bottom: 8.0, top: 8.0),
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    BorderSide(color: Color(0xFFbdc6cf)),
                                borderRadius: BorderRadius.circular(25.7),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(
                                    color: Color(0xFFbdc6cf), width: 2.0),
                                borderRadius: BorderRadius.circular(25.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      child: new FormField(
                        builder: (FormFieldState state) {
                          return firstContainer(
                            state,
                            0.0,
                            IconButton(
                              icon: Icon(Icons.keyboard_arrow_down),
                              onPressed: () {},
                            ),
                          );
                        },
                      ),
                    )
                  ],
                ),
                Container(
                  padding: EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Expanded(
                        child: new FormField(
                          builder: (FormFieldState state) {
                            return firstContainer(
                              state,
                              0.0,
                              IconButton(
                                icon: Icon(Icons.keyboard_arrow_down),
                                onPressed: () {},
                              ),
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                ),
                Container(
                  margin: EdgeInsets.only(top: 20.0),
                  child: Button(
                    Height: 45.0,
                    textValue: "ADD NEW CUSTOMER ",
                    color: Colors.white,
                    fontWeight: FontWeight.normal,
                    fontSize: 12.0,
                    onPressed: () {},
                    borderRadius: BorderRadius.circular(50),
                    bgcolor: Colors.brown,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  bool isChecked = false;

  void toggleCheckbox(bool value) {
    if (isChecked == false) {
      // Put your code here which you want to execute on CheckBox Checked event.
      setState(() {
        isChecked = true;
      });
    } else {
      // Put your code here which you want to execute on CheckBox Un-Checked event.
      setState(() {
        isChecked = false;
      });
    }
  }

  Container firstContainer(
      FormFieldState state, double topValue, Widget dataIcon) {
    return Container(
      padding: EdgeInsets.all(4.0),
      margin: EdgeInsets.only(top: topValue),
      height: 45,
      decoration: BoxDecoration(
        boxShadow: [
          BoxShadow(
            blurRadius: 13.0,
            color: Colors.black.withOpacity(.5),
            offset: Offset(1.0, 1.0),
          ),
        ],
        border: Border.all(width: 1, color: Colors.grey[600]),
        borderRadius: BorderRadius.circular(28),
        color: Colors.white,
      ),
      child: new DropdownButtonHideUnderline(
        child: Container(
          padding: EdgeInsets.only(left: 9.0),
          child: new DropdownButton(
            value: _truck,
            isDense: true,
            icon: dataIcon,
            onChanged: (String newValue) {
              setState(() {
//                          newContact.favoriteColor = newValue;
                _truck = newValue;
                state.didChange(newValue);
              });
            },
            items: _truckTypes.map((String value) {
              return new DropdownMenuItem(
                value: value,
                child: new Text(value),
              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}
