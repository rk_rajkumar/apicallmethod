import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'CustomException.dart';


class ApiProvider {
  final String _baseUrl = "https://admin.tramasys.my/api/";
  http.Response response;
  String token =
      'x1kGRB5HVEzC9ptQSHulbMq46gW7O24N82j1lZBODr1aEXuHi5qy0R5uPrRDtxaa8UakipuuXZEPnZ5z';

  Future<dynamic> get(String url, int methodType) async {
    var responseJson;
//    try {
//      final response = await http.get(_baseUrl + url);
//      responseJson = _response(response);
//    } on SocketException {
//      throw FetchDataException('No Internet connection');
//    }

    try {
      print("url ${_baseUrl + url}");
      if (methodType == 0) {
        response = await http.get(_baseUrl + url,
            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
        responseJson = _response(response);
      }

//      else if (methodType == 1) {
//        response = await http.post(url,
//            body: params,
//            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
//        responseJson = _response(response);
//      } else if (methodType == 2) {
//        response = await http.put(url,
//            body: params,
//            headers: {HttpHeaders.authorizationHeader: 'Bearer $token'});
//        responseJson = _response(response);
//      } else {
//        response = await http.delete(url);
//        responseJson = _response(response);
//      }
    } on SocketException {
//      throw FetchDataException('No Internet connection');
    }
    return responseJson;
  }

  dynamic _response(http.Response response) {
    switch (response.statusCode) {
      case 200:
        var responseJson = json.decode(response.body.toString());
        print(responseJson);
        return responseJson;
      case 400:
        throw BadRequestException(response.body.toString());
      case 401:

      case 403:
        throw UnauthorisedException(response.body.toString());
      case 500:

      default:
        throw FetchDataException(
            'Error occured while Communication with Server with StatusCode : ${response.statusCode}');
    }
  }
}
