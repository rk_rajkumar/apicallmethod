class ResponseValue<T> {
  Status status;
  T data;
  String message;

  ResponseValue.loading(this.message) : status = Status.LOADING;
  ResponseValue.completed(this.data) : status = Status.COMPLETED;
  ResponseValue.error(this.message) : status = Status.ERROR;

  @override
  String toString() {
    return "Status : $status \n Message : $message \n Data : $data";
  }
}

enum Status { LOADING, COMPLETED, ERROR }
