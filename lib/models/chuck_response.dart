// To parse this JSON data, do
//
//     final chuckResponse = chuckResponseFromJson(jsonString);

import 'dart:convert';

import 'package:meta/meta.dart';

class chuckResponse {
  final List<String> categories;
  final DateTime createdAt;
  final String iconUrl;
  final String id;
  final DateTime updatedAt;
  final String url;
  final String value;

  chuckResponse({
    @required this.categories,
    @required this.createdAt,
    @required this.iconUrl,
    @required this.id,
    @required this.updatedAt,
    @required this.url,
    @required this.value,
  });

  factory chuckResponse.fromJson(String str) =>
      chuckResponse.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory chuckResponse.fromMap(Map<String, dynamic> json) => chuckResponse(
        categories: json["categories"] == null
            ? null
            : List<String>.from(json["categories"].map((x) => x)),
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        iconUrl: json["icon_url"] == null ? null : json["icon_url"],
        id: json["id"] == null ? null : json["id"],
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        url: json["url"] == null ? null : json["url"],
        value: json["value"] == null ? null : json["value"],
      );

  Map<String, dynamic> toMap() => {
        "categories": categories == null
            ? null
            : List<dynamic>.from(categories.map((x) => x)),
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "icon_url": iconUrl == null ? null : iconUrl,
        "id": id == null ? null : id,
        "updated_at": updatedAt == null ? null : updatedAt.toIso8601String(),
        "url": url == null ? null : url,
        "value": value == null ? null : value,
      };
}
