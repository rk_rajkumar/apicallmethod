import 'dart:convert';

import 'package:meta/meta.dart';


Information informationFromJson(String str) => Information.fromJson(json.decode(str));

String informationToJson(Information data) => json.encode(data.toJson());

class Information {
  Information({
    this.status,
    this.response,
  });

  String status;
  List<Response> response;

  factory Information.fromJson(Map<String, dynamic> json) => Information(
    status: json["status"],
    response: List<Response>.from(json["response"].map((x) => Response.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "status": status,
    "response": List<dynamic>.from(response.map((x) => x.toJson())),
  };
}

class Response {
  Response({
    this.regNo,
    this.attributes,
    this.owner,
  });

  String regNo;
  String attributes;
  //List<attri> attributes;
  String owner;

  factory Response.fromJson(Map<String, dynamic> json) => Response(
    regNo: json["reg_no"],
    attributes: json["attributes"] == null ? null : json["attributes"],
    //attributes: List<attri>.from(json["attributes"].map((x) => attri.fromJson(x))),
    owner: json["owner"],
  );

  Map<String, dynamic> toJson() => {
    "reg_no": regNo,
    "attributes": attributes == null ? null : attributes,
    //"attributes": List<dynamic>.from(attributes.map((x) => x.toJson())),
    "owner": owner,
  };
}

class attri {
  attri({
    this.Type,
    this.Mileage,
    this.Capacity,
    this.Members,
    this.length,
    this.Engine,
  });

  String Type;
  String Mileage;
  String Capacity;
  String Members;
  String length;
  String Engine;

  factory attri.fromJson(Map<String, dynamic> json) => attri(
    Type: json["Type"],
    Mileage: json["Mileage"],
    Capacity: json["Capacity"],
    Members: json["Members"],
    length: json["length"],
    Engine: json["Engine"],
  );

  Map<String, dynamic> toJson() => {
    "Type": Type,
    "Mileage": Mileage,
    "Capacity": Capacity,
    "Members": Members,
    "length": length,
    "Engine": Engine,
  };
}