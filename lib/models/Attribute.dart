import 'dart:convert';

import 'package:meta/meta.dart';

class GetAttributesModel {
  GetAttributesModel({
    this.status,
    this.response,
  });

  final String status;
  final Response response;

  factory GetAttributesModel.fromJson(String str) =>
      GetAttributesModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory GetAttributesModel.fromMap(Map<String, dynamic> json) =>
      GetAttributesModel(
        status: json["status"] == null ? null : json["status"],
        response: json["response"] == null
            ? null
            : Response.fromMap(json["response"]),
      );

  Map<String, dynamic> toMap() => {
        "status": status == null ? null : status,
        "response": response == null ? null : response.toMap(),
      };
}

class Response {
  Response({
    this.smallCars,
    this.mediumCars,
    this.lightDuty4X4PickUpTruck,
    this.boxLorryLoriKotak,
    this.minibusCoach,
    this.coachMotorCoach,
  });

  final SmallCars smallCars;
  final MediumCars mediumCars;
  final LightDuty4X4PickUpTruck lightDuty4X4PickUpTruck;
  final BoxLorryLoriKotak boxLorryLoriKotak;
  final MinibusCoach minibusCoach;
  final CoachMotorCoach coachMotorCoach;

  factory Response.fromJson(String str) => Response.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory Response.fromMap(Map<String, dynamic> json) => Response(
        smallCars: json["Small cars"] == null
            ? null
            : SmallCars.fromMap(json["Small cars"]),
        mediumCars: json["Medium cars"] == null
            ? null
            : MediumCars.fromMap(json["Medium cars"]),
        lightDuty4X4PickUpTruck: json["Light-duty 4x4 pick-up truck"] == null
            ? null
            : LightDuty4X4PickUpTruck.fromMap(
                json["Light-duty 4x4 pick-up truck"]),
        boxLorryLoriKotak: json["Box Lorry (Lori Kotak)"] == null
            ? null
            : BoxLorryLoriKotak.fromMap(json["Box Lorry (Lori Kotak)"]),
        minibusCoach: json["Minibus/coach"] == null
            ? null
            : MinibusCoach.fromMap(json["Minibus/coach"]),
        coachMotorCoach: json["Coach / Motor coach"] == null
            ? null
            : CoachMotorCoach.fromMap(json["Coach / Motor coach"]),
      );

  Map<String, dynamic> toMap() => {
        "Small cars": smallCars == null ? null : smallCars.toMap(),
        "Medium cars": mediumCars == null ? null : mediumCars.toMap(),
        "Light-duty 4x4 pick-up truck": lightDuty4X4PickUpTruck == null
            ? null
            : lightDuty4X4PickUpTruck.toMap(),
        "Box Lorry (Lori Kotak)":
            boxLorryLoriKotak == null ? null : boxLorryLoriKotak.toMap(),
        "Minibus/coach": minibusCoach == null ? null : minibusCoach.toMap(),
        "Coach / Motor coach":
            coachMotorCoach == null ? null : coachMotorCoach.toMap(),
      };
}

class BoxLorryLoriKotak {
  BoxLorryLoriKotak({
    @required this.model,
    @required this.compact,
  });

  final List<String> model;
  final List<String> compact;

  factory BoxLorryLoriKotak.fromJson(String str) =>
      BoxLorryLoriKotak.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory BoxLorryLoriKotak.fromMap(Map<String, dynamic> json) =>
      BoxLorryLoriKotak(
        model: json["model"] == null
            ? null
            : List<String>.from(json["model"].map((x) => x)),
        compact: json["Compact"] == null
            ? null
            : List<String>.from(json["Compact"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "model": model == null ? null : List<dynamic>.from(model.map((x) => x)),
        "Compact":
            compact == null ? null : List<dynamic>.from(compact.map((x) => x)),
      };
}

class CoachMotorCoach {
  CoachMotorCoach({
    @required this.passengers,
  });

  final List<String> passengers;

  factory CoachMotorCoach.fromJson(String str) =>
      CoachMotorCoach.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory CoachMotorCoach.fromMap(Map<String, dynamic> json) => CoachMotorCoach(
        passengers: json["Passengers"] == null
            ? null
            : List<String>.from(json["Passengers"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "Passengers": passengers == null
            ? null
            : List<dynamic>.from(passengers.map((x) => x)),
      };
}

class LightDuty4X4PickUpTruck {
  LightDuty4X4PickUpTruck({
    @required this.engine,
    @required this.length,
  });

  final List<String> engine;
  final List<String> length;

  factory LightDuty4X4PickUpTruck.fromJson(String str) =>
      LightDuty4X4PickUpTruck.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory LightDuty4X4PickUpTruck.fromMap(Map<String, dynamic> json) =>
      LightDuty4X4PickUpTruck(
        engine: json["Engine"] == null
            ? null
            : List<String>.from(json["Engine"].map((x) => x)),
        length: json["length"] == null
            ? null
            : List<String>.from(json["length"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "Engine":
            engine == null ? null : List<dynamic>.from(engine.map((x) => x)),
        "length":
            length == null ? null : List<dynamic>.from(length.map((x) => x)),
      };
}

class MediumCars {
  MediumCars({
    @required this.capacity,
  });

  final List<String> capacity;

  factory MediumCars.fromJson(String str) =>
      MediumCars.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory MediumCars.fromMap(Map<String, dynamic> json) => MediumCars(
        capacity: json["Capacity"] == null
            ? null
            : List<String>.from(json["Capacity"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "Capacity": capacity == null
            ? null
            : List<dynamic>.from(capacity.map((x) => x)),
      };
}

class MinibusCoach {
  MinibusCoach({
    @required this.ac,
    @required this.handSanitizers,
  });

  final List<String> ac;
  final List<String> handSanitizers;

  factory MinibusCoach.fromJson(String str) =>
      MinibusCoach.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory MinibusCoach.fromMap(Map<String, dynamic> json) => MinibusCoach(
        ac: json["AC"] == null
            ? null
            : List<String>.from(json["AC"].map((x) => x)),
        handSanitizers: json["Hand Sanitizers"] == null
            ? null
            : List<String>.from(json["Hand Sanitizers"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "AC": ac == null ? null : List<dynamic>.from(ac.map((x) => x)),
        "Hand Sanitizers": handSanitizers == null
            ? null
            : List<dynamic>.from(handSanitizers.map((x) => x)),
      };
}

class SmallCars {
  SmallCars({
    @required this.capacity,
    @required this.compact,
  });

  final List<String> capacity;
  final List<String> compact;

  factory SmallCars.fromJson(String str) => SmallCars.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory SmallCars.fromMap(Map<String, dynamic> json) => SmallCars(
        capacity: json["Capacity"] == null
            ? null
            : List<String>.from(json["Capacity"].map((x) => x)),
        compact: json["Compact"] == null
            ? null
            : List<String>.from(json["Compact"].map((x) => x)),
      );

  Map<String, dynamic> toMap() => {
        "Capacity": capacity == null
            ? null
            : List<dynamic>.from(capacity.map((x) => x)),
        "Compact":
            compact == null ? null : List<dynamic>.from(compact.map((x) => x)),
      };
}
