import 'dart:convert';

import 'package:meta/meta.dart';

class FilterModel {
  FilterModel({
    @required this.compact,
    @required this.capacity,
    @required this.model,
    @required this.engine,
  });

  final String compact;
  final String capacity;
  final String model;
  final String engine;

  factory FilterModel.fromJson(String str) =>
      FilterModel.fromMap(json.decode(str));

  String toJson() => json.encode(toMap());

  factory FilterModel.fromMap(Map<String, dynamic> json) => FilterModel(
        compact: json["Compact"] == null ? null : json["Compact"],
        capacity: json["Capacity"] == null ? null : json["Capacity"],
        model: json["model"] == null ? null : json["model"],
        engine: json["Engine"] == null ? null : json["Engine"],
      );

  Map<String, dynamic> toMap() => {
        "Compact": compact == null ? null : compact,
        "Capacity": capacity == null ? null : capacity,
        "model": model == null ? null : model,
        "Engine": engine == null ? null : engine,
      };
}
