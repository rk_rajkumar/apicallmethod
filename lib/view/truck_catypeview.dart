import 'package:flutter/material.dart';
import 'package:flutterlorryapikhit/blocs/firsttruck_bloc.dart';
import 'package:flutterlorryapikhit/networking/Response.dart';
import 'package:flutterlorryapikhit/view/constantvalues.dart';
import 'package:flutterlorryapikhit/view/specificList.dart';
import 'package:flutterlorryapikhit/view/truck_view.dart';

import 'newmodel.dart';

const buttonColor = const Color(0xff311716);
const dividerColor = const Color(0xffD9D9D9);
const bgColor = const Color(0xff00000029);
const MainTextColor = const Color(0xff000000);
const SubTextColor = const Color(0xff414141);

class truck_catypeview extends StatefulWidget {
  @override
  _GetTruckView createState() => _GetTruckView();
}

class _GetTruckView extends State<truck_catypeview> {
  firsttruck_bloc _bloc;
  List<FilterTypeItem> filterTypeItem;

  @override
  void initState() {
    super.initState();
    _bloc = firsttruck_bloc("1");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        automaticallyImplyLeading: false,
        titleSpacing: 5,
        title: Row(
          children: <Widget>[
            new IconButton(
              icon: new Icon(
                Icons.filter_list,
                color: Colors.black,
              ),
              iconSize: 25,
              onPressed: () {},
            ),
            Text('Search Filter',
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: MainTextColor)),
          ],
        ),
//        backgroundColor: Color(0xFF333333),
        backgroundColor: Color.fromRGBO(230, 230, 230, 1),
      ),
//      backgroundColor: Color(0xFF333333),
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchfirstCategories("1"),
        child: StreamBuilder<ResponseValue<newmodel>>(
          stream: _bloc.firsttruckListStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  constantvalues.setmulitselectedlist(new Map());

                  return TruckList(categoryList: snapshot.data.data);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchfirstCategories("1"),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}

class TruckList extends StatefulWidget {
  newmodel categoryList;

  bool isSelected = false;
  String _isOneSelected = "";
  List<String> reportList;

  Function(List<String>) onSelectionChanged; // +added
  TruckList(
      {Key key, this.categoryList, this.reportList, this.onSelectionChanged})
      : super(key: key);

  @override
  _TruckList createState() => _TruckList();
}

class _TruckList extends State<TruckList> {
  List<String> selectedChoices = List();
  bool isSelected = false;
  List<String> selindex = new List();
  Map<String, List<String>> selectedlist = new Map();
  @override
  Widget build(BuildContext context) {
    selindex = constantvalues.comonselindex;
    return new Scaffold(
//      backgroundColor: Color(0xFF202020),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            ListView.builder(
              itemBuilder: (context, index) {
                return Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: 0.0,
                      vertical: 1.0,
                    ),
                    child: InkWell(
                        child: SizedBox(
                      child: Container(
                        //color: Color(0xFF333333),
                        color: Colors.white,
                        alignment: Alignment.center,
                        child: SingleChildScrollView(
                            child: Column(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Container(
                                  margin: EdgeInsets.only(
                                      left: 20, top: 20, bottom: 20),
                                  child: Text(
                                    "${widget.categoryList.response.lightDuty4x4PickUpTruck[index].typename}",
                                    style: TextStyle(
                                        backgroundColor: Colors.white,
                                        color: Colors.black,
                                        fontSize: 20,
                                        fontWeight: FontWeight.w700,
                                        fontFamily: 'Roboto'),
                                    textAlign: TextAlign.left,
                                  )),
                            ),
                            ListView.builder(
                              itemBuilder: (context, subindex) {
                                @override
                                State<StatefulWidget> createState() {
                                  // TODO: implement createState
                                  throw UnimplementedError();
                                }

                                return Padding(
                                    padding: EdgeInsets.symmetric(
                                      horizontal: 0.0,
                                      vertical: 1.0,
                                    ),
                                    child: InkWell(
                                        onTap: () {
//                                                    constantvalues.setselectedcapacity(categoryList.response.lightDuty4x4PickUpTruck[index].typename);
//                                                    constantvalues.setselectedvalues(categoryList.response.lightDuty4x4PickUpTruck[index].items[subindex]);
//                                                    Navigator.push(
//                                                      context,
//                                                      MaterialPageRoute(builder: (context) => TruckView()),
//                                                    );
                                        },
                                        child: SizedBox(
                                          height: 65,
                                          child: Container(
                                            //color: Color(0xFF333333),
                                            color: Colors.white,
                                            alignment: Alignment.centerLeft,
                                            margin: EdgeInsets.only(left: 35),
                                            child: Column(
                                              children: [
                                                ChoiceChip(
                                                  label: Text(
                                                      "${widget.categoryList.response.lightDuty4x4PickUpTruck[index].items[subindex]}"),
                                                  labelStyle: TextStyle(
                                                      color: MainTextColor),
                                                  selectedColor: Colors.green,
                                                  backgroundColor:
                                                      Colors.transparent,
//                                                  selected: widget
//                                                          ._isOneSelected ==
//                                                      "${widget.categoryList.response.lightDuty4x4PickUpTruck[index].items[subindex]}",
                                                  selected: selindex.contains(
                                                      "$index-$subindex"),
                                                  shape: RoundedRectangleBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              0.0),
                                                      side: BorderSide(
                                                          color: Colors.grey,
                                                          style: BorderStyle
                                                              .solid)),
                                                  onSelected: (selected) {
                                                    setState(() {
                                                      isSelected = selected;
                                                      if (selected) {
                                                        selindex.add(
                                                            "$index-$subindex");
                                                      } else {
                                                        selindex.remove(
                                                            "$index-$subindex");
                                                      }
                                                    });
//                                                              setState(() {
//                                                                _isOneSelected = "${categoryList.response.lightDuty4x4PickUpTruck[index].items[subindex]}";
//                                                              });
                                                    String typename = widget
                                                        .categoryList
                                                        .response
                                                        .lightDuty4x4PickUpTruck[
                                                            index]
                                                        .typename;
                                                    String typevalue = widget
                                                        .categoryList
                                                        .response
                                                        .lightDuty4x4PickUpTruck[
                                                            index]
                                                        .items[subindex];
                                                    selectedlist =
                                                        constantvalues
                                                            .multiselectedlist;
                                                    List<String> itemlist =
                                                        selectedlist[
                                                            "$typename"];

                                                    if (itemlist == null) {
                                                      if (selindex.contains(
                                                          "$index-$subindex")) {
                                                        List<String> selitem =
                                                            new List<String>();
                                                        selitem
                                                            .add("$typevalue");
                                                        selectedlist[typename] =
                                                            selitem;
                                                      }
                                                    } else {
                                                      if (selindex.contains(
                                                          "$index-$subindex")) {
                                                        itemlist
                                                            .add("$typevalue");
                                                        selectedlist[typename] =
                                                            itemlist;
                                                      } else {
                                                        itemlist.remove(
                                                            "$typevalue");
                                                        selectedlist[typename] =
                                                            itemlist;
                                                      }
                                                    }
                                                    constantvalues
                                                        .setselectedcapacity(widget
                                                            .categoryList
                                                            .response
                                                            .lightDuty4x4PickUpTruck[
                                                                index]
                                                            .typename);
                                                    constantvalues
                                                        .setselectedvalues(widget
                                                            .categoryList
                                                            .response
                                                            .lightDuty4x4PickUpTruck[
                                                                index]
                                                            .items[subindex]);
                                                  },
                                                ),

//Kumarrrrrrr
//                                                          Padding(
//                                                            padding: EdgeInsets.fromLTRB(35, 0, 0, 0),
//                                                            child: Text(
//                                                              "${categoryList.response.lightDuty4x4PickUpTruck[index].items[subindex]}",
//                                                              style: TextStyle(
//                                                                  color: Colors.black,
//                                                                  fontSize: 20,
//                                                                  fontWeight: FontWeight.w500,
//                                                                  fontFamily: 'Roboto'),
//                                                              textAlign: TextAlign.left,
//                                                            ),
//                                                          ),
                                              ],
                                            ),
                                          ),
                                        )));
                              },
                              itemCount: widget.categoryList.response
                                  .lightDuty4x4PickUpTruck[index].items.length,
                              shrinkWrap: true,
                              physics: ClampingScrollPhysics(),
                            ),
                            Container(
                              height: 2,
                              margin: EdgeInsets.only(left: 15, right: 15),
                              color: dividerColor,
                            )
                          ],
                        )),
                      ),
                    )));
              },
              itemCount:
                  widget.categoryList.response.lightDuty4x4PickUpTruck.length,
              shrinkWrap: true,
              physics: ClampingScrollPhysics(),
            ),
            Align(
              alignment: Alignment.bottomLeft,
              child: Container(
                //color: Colors.red,
                //width: MediaQuery.of(context).size.width/2,
                margin: EdgeInsets.only(left: 15, top: 20, bottom: 20),
                height: 50,
                width: 150,
                child: FlatButton(
                  child: new Text(
                    "Apply Filter",
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 16),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  onPressed: () {
                    print(" Selection list $selectedlist  ");
                    constantvalues.setmulitselectedlist(selectedlist);
                    constantvalues.setcomonselindex(selindex);
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => TruckView()),
                    );
                  },
                  color: buttonColor,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  _buildChoiceList() {
    List<Widget> choices = List();
    widget.reportList.forEach((item) {
      choices.add(Container(
        padding: const EdgeInsets.all(2.0),
        child: ChoiceChip(
          label: Text(item),
          selected: selectedChoices.contains(item),
          onSelected: (selected) {
            setState(() {
              selectedChoices.contains(item)
                  ? selectedChoices.remove(item)
                  : selectedChoices.add(item);
              widget.onSelectionChanged(selectedChoices); // +added
            });
          },
        ),
      ));
    });
    return choices;
  }
}

class Error extends StatelessWidget {
  final String errorMessage;

  final Function onRetryPressed;

  const Error({Key key, this.errorMessage, this.onRetryPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 8),
          RaisedButton(
            color: Colors.white,
            child: Text('Retry', style: TextStyle(color: Colors.black)),
            onPressed: onRetryPressed,
          )
        ],
      ),
    );
  }
}

class Loading extends StatelessWidget {
  final String loadingMessage;

  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),
        ],
      ),
    );
  }
}
