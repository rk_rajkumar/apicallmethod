class newmodel {
  String status;
  Response response;

  newmodel({this.status, this.response});

  newmodel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    response = json['response'] != null
        ? new Response.fromJson(json['response'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.response != null) {
      data['response'] = this.response.toJson();
    }
    return data;
  }
}

class Response {
  List<LightDuty4x4PickUpTruck> lightDuty4x4PickUpTruck;

  Response({this.lightDuty4x4PickUpTruck});

  Response.fromJson(Map<String, dynamic> json) {
    if (json['Light-duty 4x4 pick-up truck'] != null) {
      lightDuty4x4PickUpTruck = new List<LightDuty4x4PickUpTruck>();
      json['Light-duty 4x4 pick-up truck'].forEach((v) {
        lightDuty4x4PickUpTruck.add(new LightDuty4x4PickUpTruck.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.lightDuty4x4PickUpTruck != null) {
      data['Light-duty 4x4 pick-up truck'] =
          this.lightDuty4x4PickUpTruck.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class LightDuty4x4PickUpTruck {
  String typename;
  List<String> items;

  LightDuty4x4PickUpTruck({this.typename, this.items});

  LightDuty4x4PickUpTruck.fromJson(Map<String, dynamic> json) {
    typename = json['typename'];
    items = json['items'].cast<String>();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['typename'] = this.typename;
    data['items'] = this.items;
    return data;
  }
}