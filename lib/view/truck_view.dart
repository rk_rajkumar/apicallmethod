import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutterlorryapikhit/blocs/truck_bloc.dart';
import 'package:flutterlorryapikhit/models/Info.dart';
import 'package:flutterlorryapikhit/networking/Response.dart';
import 'package:flutterlorryapikhit/view/constantvalues.dart';

import 'truck_catypeview.dart';

class TruckView extends StatefulWidget {
  @override
  _GetTruckView createState() => _GetTruckView();
}

class _GetTruckView extends State<TruckView> {
  TruckBloc _bloc;

  @override
  void initState() {
    super.initState();
    _bloc = TruckBloc();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(child: truck_catypeview()),
      appBar: AppBar(
        elevation: 0.0,
        //automaticallyImplyLeading: false,
        titleSpacing: 0,
        title: Text('Vehicles',
            style: TextStyle(color: Colors.white, fontSize: 20)),
//        backgroundColor: Color(0xFF333333),
        backgroundColor: Colors.red,
//        leading: IconButton(
//          icon: Icon(Icons.accessible),
//          onPressed: () => Drawer(child: truck_catypeview()),
//        ),
//        leading: new IconButton(
//          icon: new Icon(
//            Icons.filter_list,
//            color: Colors.white,
//            size: 30.0,
//          ),
//          onPressed: () async {
//            Navigator.push(
//              context,
//              MaterialPageRoute(
//                  builder: (context) => truck_catypeview()),
//            );
//          },
//        ),
      ),
//      backgroundColor: Color(0xFF333333),
      backgroundColor: Colors.white,
      body: RefreshIndicator(
        onRefresh: () => _bloc.fetchCategories(),
        child: StreamBuilder<ResponseValue<Information>>(
          stream: _bloc.truckListStream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              switch (snapshot.data.status) {
                case Status.LOADING:
                  return Loading(loadingMessage: snapshot.data.message);
                  break;
                case Status.COMPLETED:
                  return TruckList(categoryList: snapshot.data.data);
                  break;
                case Status.ERROR:
                  return Error(
                    errorMessage: snapshot.data.message,
                    onRetryPressed: () => _bloc.fetchCategories(),
                  );
                  break;
              }
            }
            return Container();
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _bloc.dispose();
    super.dispose();
  }
}

class TruckList extends StatelessWidget {
  final Information categoryList;

  const TruckList({Key key, this.categoryList}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    List<Response> tempcategoryList = new List<Response>();

    List<attri> tempListattri = new List<attri>();

    if (constantvalues.multiselectedlist != null &&
        constantvalues.multiselectedlist.isNotEmpty) {
      categoryList.response.forEach((element) {
        print("JSon Sting ${element.attributes}");

        if (element.attributes != null &&
            element.attributes.isNotEmpty &&
            element.attributes != "null") {
          print("with in if JSon Sting ${element.attributes}");
          var resitem = jsonDecode(element.attributes);
          constantvalues.multiselectedlist.forEach((key, value) {
            print(" multiselectvalue  $key  $value");
            value.forEach((typeelement) {
              if (typeelement == "${resitem[key]}") {
                //if (!tempcategoryList.contains(element)) {
                  tempcategoryList.add(element);
                //}
              }
            });
          });
        }
      });
    } else {
      tempcategoryList = categoryList.response;
    }
//    else{
//      categoryList.response.forEach((element) {
//        if(element.attributes!=null&&element.attributes.isNotEmpty&&element.attributes!="null") {
//          tempcategoryList.add(element);
//          print("with in if JSon tempcategoryList $tempcategoryList");
//        }
//      });
//    }
    return new Scaffold(
      //backgroundColor: Color(0xFF202020),
      backgroundColor: Colors.white,
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Padding(
              padding: EdgeInsets.symmetric(
                horizontal: 0.0,
                vertical: 1.0,
              ),
              child: InkWell(
//                  onTap: () {
//                    Navigator.of(context).push(MaterialPageRoute(
//                        builder: (context) =>
//                            ShowChuckyJoke(categoryList.categories[index])));
//                  },
                  child: SizedBox(
                child: Container(
                  margin: EdgeInsets.only(top: 10, left: 20, right: 20),
                  //color: Color(0xFF333333),
                  color: Colors.white,
                  alignment: Alignment.centerLeft,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                        child: Text(
                          "Reg No : ${tempcategoryList[index].regNo}",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 20,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Roboto'),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(5, 0, 0, 5),
                        child: Text(
                          "Owner : ${tempcategoryList[index].owner}",
                          style: TextStyle(
                              color: Colors.black,
                              fontSize: 18,
                              fontWeight: FontWeight.w500,
                              fontFamily: 'Roboto'),
                          textAlign: TextAlign.left,
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(5, 0, 0, 15),
                        child: tempcategoryList[index].attributes != null
                            ? Text(
                                //"${tempcategoryList[index].attributes}",
//                              "${tempcategoryList[index].attributes.substring(1, tempcategoryList[index].attributes.indexOf('}'))}",
                                "",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'Roboto'),
                                textAlign: TextAlign.left,
                              )
                            : Text(
                                "welcome",
                                style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 18,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'Roboto'),
                                textAlign: TextAlign.left,
                              ),
                      ),
                      Container(
                        height: 2,
                        //margin: EdgeInsets.only(left: 10,right: 10),
                        color: dividerColor,
                      )
                    ],
                  ),
                ),
              )));
        },
        itemCount: tempcategoryList.length,
        shrinkWrap: true,
        physics: ClampingScrollPhysics(),
      ),
    );
  }
}

class Error extends StatelessWidget {
  final String errorMessage;

  final Function onRetryPressed;

  const Error({Key key, this.errorMessage, this.onRetryPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            errorMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 18,
            ),
          ),
          SizedBox(height: 8),
          RaisedButton(
            color: Colors.white,
            child: Text('Retry', style: TextStyle(color: Colors.black)),
            onPressed: onRetryPressed,
          )
        ],
      ),
    );
  }
}

class Loading extends StatelessWidget {
  final String loadingMessage;

  const Loading({Key key, this.loadingMessage}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Text(
            loadingMessage,
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Colors.white,
              fontSize: 24,
            ),
          ),
          SizedBox(height: 24),
          CircularProgressIndicator(
            valueColor: AlwaysStoppedAnimation<Color>(Colors.white),
          ),
        ],
      ),
    );
  }
}
