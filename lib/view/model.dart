import 'package:flutter/material.dart';

class Model {
  final String meeting_id;
  final String meeting_type;
  final String meeting_status;
  final String meeting_title;
  final String meeting_brand;
  final String meeting_product;
  final String speaker_count;
  final String attendee_invited_count;
  final String attendee_confirmed_count;
  final String meeting_date;
  final String meeting_time;

  Model(
      {this.meeting_id,
      this.meeting_type,
      this.meeting_status,
      this.meeting_title,
      this.meeting_brand,
      this.meeting_product,
      this.speaker_count,
      this.attendee_invited_count,
      this.attendee_confirmed_count,
      this.meeting_date,
      this.meeting_time});
}

//class Result extends StatelessWidget {
//  @override
//  Widget build(BuildContext context) {
//    final size = MediaQuery.of(context).size;
//    final width = size.width;
//    final height = size.height;
//    final GlobalKey globalKey = GlobalKey();
//
//    void getWH() {
//      final cardWidth = globalKey.currentContext.size.width;
//      final cardHeight = globalKey.currentContext.size.height;
//      print('Card width is $cardWidth, height is $cardHeight');
//    }
//
//    //final halfwidth = MediaQuery.of(context).size.width / 2;
//    return Scaffold(
//      //appBar: AppBar(title: Text('Employee List')),
//      body: Container(
//        padding: EdgeInsets.all(5.0),
//        decoration: BoxDecoration(
//          borderRadius: BorderRadius.circular(25.0),
//        ),
//        child: Card(
//          key: globalKey,
//          child: Container(
//            padding:
//                EdgeInsets.only(left: 5.0, right: 5.0, top: 5.0, bottom: 15.0),
//            child: Row(
//              //crossAxisAlignment: CrossAxisAlignment.end,
//              children: <Widget>[
//                Container(
//                  height: height / 8,
//                  child: VerticalDivider(
//                    color: Colors.teal,
//                    thickness: 5.0,
//                  ),
//                ),
//                Column(
//                  mainAxisSize: MainAxisSize.min,
//                  crossAxisAlignment: CrossAxisAlignment.start,
//                  children: <Widget>[
//                    Row(
//                      children: <Widget>[
//                        Text("ID: "),
//                        Container(
//                          margin: const EdgeInsets.all(2.0),
//                          padding: const EdgeInsets.all(2.0),
//                          decoration: BoxDecoration(
//                            border: Border.all(color: Colors.blueAccent),
//                            borderRadius: BorderRadius.circular(4),
//                          ),
//                          child: Text(
//                            "Meeting Type",
//                            textAlign: TextAlign.center,
//                          ),
//                        ),
//                        Container(
//                          margin: const EdgeInsets.all(2.0),
//                          padding: const EdgeInsets.all(2.0),
//                          decoration: BoxDecoration(
//                            border: Border.all(color: Colors.blueAccent),
//                            borderRadius: BorderRadius.circular(4),
//                          ),
//                          child: Text(
//                            "Meeting Status",
//                            textAlign: TextAlign.center,
//                          ),
//                        ),
//                      ],
//                    ),
//                    SizedBox(
//                      height: 3.0,
//                    ),
//                    Text(
//                      "Meeting title",
//                      style:
//                          TextStyle(fontWeight: FontWeight.bold, fontSize: 17),
//                    ),
//                    SizedBox(
//                      height: 10.0,
//                    ),
//                    Row(
//                      children: <Widget>[
//                        Text("Ipsen"),
//                        InkWell(
//                            child: Icon(
//                          Icons.arrow_forward_ios,
//                          color: Colors.teal,
//                          size: 15.0,
//                        )),
//                        Text("Somatuline Depot")
//                      ],
//                    ),
//                    SizedBox(
//                      height: 7.0,
//                    ),
//                    Container(
//                      width: width / 2,
//                      child: Divider(
//                        color: Colors.grey,
//                        //height: 100.0,
//                        thickness: 2.0,
//                        //indent: 10.0,
//                        //endIndent: 100.0,
//                      ),
//                    ),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                      children: <Widget>[
//                        Text(
//                          "Speaker: ",
//                          style: TextStyle(fontWeight: FontWeight.bold),
//                        ),
//                        Text("Date: "),
//                      ],
//                    ),
//                    Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Text(
//                          "Attendees Invited: ",
//                          style: TextStyle(
//                              fontWeight: FontWeight.bold, color: Colors.teal),
//                        ),
//                        Text("Time: "),
//                      ],
//                    ),
//                    Row(
//                      children: <Widget>[
//                        Text("Attendees Confirmed: ",
//                            style: TextStyle(fontWeight: FontWeight.bold)),
//                      ],
//                    )
//                  ],
//                ),
//              ],
//            ),
//          ),
//        ),
//      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: getWH,
//        child: Icon(Icons.adjust),
//      ),
//    );
//  }
//}
