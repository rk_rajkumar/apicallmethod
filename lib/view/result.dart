import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'model.dart';

class CardView extends StatefulWidget {
  @override
  _CardViewState createState() => _CardViewState();
}

class _CardViewState extends State<CardView> {
  List<Model> cardItems = [
    Model(
        meeting_id: "21",
        meeting_type: "Live Out Office",
        meeting_status: "Awaiting speaker reply",
        meeting_title: "A Clinical Update in Cardiology",
        meeting_brand: "Ipsen",
        meeting_product: "Somatuline Depot",
        speaker_count: "3",
        attendee_invited_count: "1",
        attendee_confirmed_count: "0",
        meeting_date: "August 20, 2020",
        meeting_time: "08:30 AM EST"),
    Model(
        meeting_id: "22",
        meeting_type: "Virtual Office",
        meeting_status: "Awaiting speaker reply",
        meeting_title: "A Clinical Update in Gasteroenteropancreatic",
        meeting_brand: "Ipsen",
        meeting_product: "Somatuline Depot",
        speaker_count: "3",
        attendee_invited_count: "1",
        attendee_confirmed_count: "0",
        meeting_date: "August 20, 2020",
        meeting_time: "08:30 AM EST"),
    Model(
        meeting_id: "23",
        meeting_type: "Live Out Office",
        meeting_status: "Awaiting speaker reply",
        meeting_title: "A Clinical Update in Cardiology",
        meeting_brand: "Ipsen",
        meeting_product: "Somatuline Depot",
        speaker_count: "3",
        attendee_invited_count: "1",
        attendee_confirmed_count: "0",
        meeting_date: "August 20, 2020",
        meeting_time: "08:30 AM EST"),
    Model(
        meeting_id: "24",
        meeting_type: "Live Out Office",
        meeting_status: "Awaiting speaker reply",
        meeting_title: "A Clinical Update in Cardiology",
        meeting_brand: "Ipsen",
        meeting_product: "Somatuline Depot",
        speaker_count: "3",
        attendee_invited_count: "1",
        attendee_confirmed_count: "0",
        meeting_date: "August 20, 2020",
        meeting_time: "08:30 AM EST"),
    Model(
        meeting_id: "25",
        meeting_type: "Live Out Office",
        meeting_status: "Awaiting speaker reply",
        meeting_title: "A Clinical Update in Cardiology",
        meeting_brand: "Ipsen",
        meeting_product: "Somatuline Depot",
        speaker_count: "3",
        attendee_invited_count: "1",
        attendee_confirmed_count: "0",
        meeting_date: "August 20, 2020",
        meeting_time: "08:30 AM EST"),
    Model(
        meeting_id: "26",
        meeting_type: "Live Out Office",
        meeting_status: "Awaiting speaker reply",
        meeting_title: "A Clinical Update in Cardiology",
        meeting_brand: "Ipsen",
        meeting_product: "Somatuline Depot",
        speaker_count: "3",
        attendee_invited_count: "1",
        attendee_confirmed_count: "0",
        meeting_date: "August 20, 2020",
        meeting_time: "08:30 AM EST"),
    Model(
        meeting_id: "27",
        meeting_type: "Live Out Office",
        meeting_status: "Awaiting speaker reply",
        meeting_title: "A Clinical Update in Cardiology",
        meeting_brand: "Ipsen",
        meeting_product: "Somatuline Depot",
        speaker_count: "3",
        attendee_invited_count: "1",
        attendee_confirmed_count: "0",
        meeting_date: "August 20, 2020",
        meeting_time: "08:30 AM EST"),
  ];

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final width = size.width;
    final height = size.height;
    final GlobalKey globalKey = GlobalKey();

    void getWH() {
      final cardWidth = globalKey.currentContext.size.width;
      final cardHeight = globalKey.currentContext.size.height;
      print('Card width is $cardWidth, height is $cardHeight');
    }

    return Scaffold(
      //appBar: AppBar(title: Text('Employee List')),
      body: Container(
        padding: EdgeInsets.all(5.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(25.0),
        ),
        child: ListView.builder(
            itemCount: cardItems.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: <Widget>[
                  Card(
                    //key: globalKey,
                    child: Container(
                      padding:
                          EdgeInsets.only(right: 5.0, top: 5.0, bottom: 5.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(2),
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            child: VerticalDivider(
                              color: Colors.teal,
                              thickness: 5.0,
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("ID: ${cardItems[index].meeting_id}"),
                              Container(
                                margin: const EdgeInsets.all(2.0),
                                padding: const EdgeInsets.all(2.0),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.blueAccent),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Text(cardItems[index].meeting_type),
                              ),
                              Container(
                                margin: const EdgeInsets.all(2.0),
                                padding: const EdgeInsets.all(2.0),
                                decoration: BoxDecoration(
                                  border: Border.all(color: Colors.blueAccent),
                                  borderRadius: BorderRadius.circular(4),
                                ),
                                child: Text(cardItems[index].meeting_status),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 3.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                cardItems[index].meeting_title,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 17),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          Row(
                            children: <Widget>[
                              Text(cardItems[index].meeting_brand),
                              InkWell(
                                  child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.teal,
                                size: 15.0,
                              )),
                              Text(cardItems[index].meeting_product)
                            ],
                          ),
                          SizedBox(
                            height: 10.0,
                          ),
                          //width: width,
                          Divider(
                            color: Colors.black,
                            height: 2,
                          ),

                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Speakers: ${cardItems[index].speaker_count}",
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              Text(cardItems[index].meeting_date),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Attendees: ${cardItems[index].attendee_invited_count}",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.teal),
                              ),
                              Text(
                                cardItems[index].meeting_time,
                                textAlign: TextAlign.center,
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              Text(
                                  "Attendees Confirmed: ${cardItems[index].attendee_confirmed_count}",
                                  style:
                                      TextStyle(fontWeight: FontWeight.bold)),
                            ],
                          ),
                          SizedBox(
                            height: 10,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              );
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: getWH,
        child: Icon(Icons.adjust),
      ),
    );
  }
}
