import 'dart:async';

import 'package:flutterlorryapikhit/networking/Response.dart';
import 'package:flutterlorryapikhit/repository/truck_repository.dart';
import 'package:flutterlorryapikhit/view/newmodel.dart';

class firsttruck_bloc {
  TruckRepository _truckRepository;
  StreamController _truckListController;

  StreamSink<ResponseValue<newmodel>> get truckListSink =>
      _truckListController.sink;

  Stream<ResponseValue<newmodel>> get truckListStream =>
      _truckListController.stream;

  Stream<ResponseValue<newmodel>> get firsttruckListStream =>
      _truckListController.stream;

  firsttruck_bloc(String attributeKey) {
    _truckListController = StreamController<ResponseValue<newmodel>>();
    _truckRepository = TruckRepository();
    fetchfirstCategories(attributeKey);
  }

  fetchfirstCategories(String attributeKey) async {
    truckListSink.add(ResponseValue.loading('Getting truck Categories.'));
    try {
      newmodel chuckCats = await _truckRepository.fetchfirstapi(attributeKey);
      truckListSink.add(ResponseValue.completed(chuckCats));
    } catch (e) {
      truckListSink.add(ResponseValue.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _truckListController?.close();
  }
}
