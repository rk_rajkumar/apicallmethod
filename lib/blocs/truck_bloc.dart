import 'dart:async';

import 'package:flutterlorryapikhit/models/Info.dart';
import 'package:flutterlorryapikhit/models/chuck_categories.dart';
import 'package:flutterlorryapikhit/networking/Response.dart';
import 'package:flutterlorryapikhit/repository/chuck_category_repository.dart';
import 'package:flutterlorryapikhit/repository/truck_repository.dart';
import 'package:flutterlorryapikhit/view/newmodel.dart';

class TruckBloc {
  TruckRepository _truckRepository;
  StreamController _truckListController;

  StreamSink<ResponseValue<Information>> get truckListSink =>
      _truckListController.sink;

  Stream<ResponseValue<Information>> get truckListStream =>
      _truckListController.stream;

  Stream<ResponseValue<newmodel>> get firsttruckListStream =>
      _truckListController.stream;

  TruckBloc() {
    _truckListController = StreamController<ResponseValue<Information>>();
    _truckRepository = TruckRepository();
    fetchCategories();
  }



  fetchCategories() async {
    truckListSink.add(ResponseValue.loading('Getting truck Categories.'));
    try {
      Information chuckCats = await _truckRepository.fetchChuckCategoryData();
      truckListSink.add(ResponseValue.completed(chuckCats));
    } catch (e) {
      truckListSink.add(ResponseValue.error(e.toString()));
      print(e);
    }
  }




  dispose() {
    _truckListController?.close();
  }
}
