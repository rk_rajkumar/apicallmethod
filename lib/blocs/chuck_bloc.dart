import 'dart:async';

import 'package:flutterlorryapikhit/models/chuck_response.dart';
import 'package:flutterlorryapikhit/networking/Response.dart';
import 'package:flutterlorryapikhit/repository/chuck_repository.dart';

class ChuckBloc {
  ChuckRepository _chuckRepository;
  StreamController _chuckDataController;

  StreamSink<ResponseValue<chuckResponse>> get chuckDataSink =>
      _chuckDataController.sink;

  Stream<ResponseValue<chuckResponse>> get chuckDataStream =>
      _chuckDataController.stream;

  ChuckBloc(String category) {
    _chuckDataController = StreamController<ResponseValue<chuckResponse>>();
    _chuckRepository = ChuckRepository();
    fetchChuckyJoke(category);
  }

  fetchChuckyJoke(String category) async {
    chuckDataSink.add(ResponseValue.loading('Getting a Chucky joke!'));
    try {
      chuckResponse chuckJoke = await _chuckRepository.fetchChuckJoke(category);
      chuckDataSink.add(ResponseValue.completed(chuckJoke));
    } catch (e) {
      chuckDataSink.add(ResponseValue.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _chuckDataController?.close();
  }
}
