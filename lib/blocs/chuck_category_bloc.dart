import 'dart:async';

import 'package:flutterlorryapikhit/models/chuck_categories.dart';
import 'package:flutterlorryapikhit/networking/Response.dart';
import 'package:flutterlorryapikhit/repository/chuck_category_repository.dart';

class ChuckCategoryBloc {
  ChuckCategoryRepository _chuckRepository;
  StreamController _chuckListController;

  StreamSink<ResponseValue<chuckCategories>> get chuckListSink =>
      _chuckListController.sink;

  Stream<ResponseValue<chuckCategories>> get chuckListStream =>
      _chuckListController.stream;

  ChuckCategoryBloc() {
    _chuckListController = StreamController<ResponseValue<chuckCategories>>();
    _chuckRepository = ChuckCategoryRepository();
    fetchCategories();
  }

  fetchCategories() async {
    chuckListSink.add(ResponseValue.loading('Getting Chuck Categories.'));
    try {
      chuckCategories chuckCats =
          await _chuckRepository.fetchChuckCategoryData();
      chuckListSink.add(ResponseValue.completed(chuckCats));
    } catch (e) {
      chuckListSink.add(ResponseValue.error(e.toString()));
      print(e);
    }
  }

  dispose() {
    _chuckListController?.close();
  }
}
